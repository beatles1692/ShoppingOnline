﻿using System.Web.Mvc;
using ShoppingOnline.Models;

namespace ShoppingOnline.Controllers
{
    public class AuthenticationController:Controller
    {
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View("Login1");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel login)
        {
            //ToDo:Should have a mechanism to authenticate
            return Redirect(login.ReturnUrl);
        }


    }
}